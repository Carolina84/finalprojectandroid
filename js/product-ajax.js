$( document ).ready(function() {

manageData();

/* manage data list */
function manageData() {
    $.ajax({
        dataType: 'json',
        url: 'api/products/getData.php'
    }).done(function(data){
    	manageRow(data.data);
    });

}

function manageRow(data) {
	var	rows = '';
	$.each( data, function( key, value ) {
	  	rows = rows + '<tr>';	   
      	rows = rows + '<td>'+value.title +'</td>';
	  	rows = rows + '<td>'+value.abstract+'</td>';
        rows = rows + '<td>'+value.description+'</td>';
        rows = rows + '<td>'+value.categorydescription+'</td>';
        rows = rows + '<td>'+value.price+'</td>';
        rows = rows + '<td>'+value.available+'</td>';
	  	rows = rows + '<td data-id="'+value.idProduct+'">';
        rows = rows + '<button data-toggle="modal" data-target="#edit-item" class="btn btn-primary edit-item">Edit</button> ';
        rows = rows + '<button class="btn btn-danger remove-item">Delete</button>';
        rows = rows + '</td>';
	  	rows = rows + '</tr>';
	});

	$("tbody").html(rows);
}

/* Create new Item */
$(".crud-submit").click(function(e){
    e.preventDefault();
    var form_action = $("#create-item").find("form").attr("action");
    var title = $("#create-item").find("input[name='title']").val();
    var abstract = $("#create-item").find("input[name='abstract']").val();
    var price = $("#create-item").find("input[name='price']").val();
    var available = $("#create-item").find("select[name='available']").val();
    var description = $("#create-item").find("textarea[name='description']").val();
    var idCategory = $("#create-item").find("select[id='idCategoryCreate']").val();
    

    if (idCategory != ''  &&   title != '' && price != '') {
        $.ajax({
            dataType: 'json',
            type:'POST',
            url: form_action,
            data:{title:title, abstract:abstract,price:price,available:available,description:description,idCategory:idCategory}
        }).done(function(data){            
            $(".modal").modal('hide');
            $("#create-item").find("input[name='title']").val("");
            $("#create-item").find("input[name='abstract']").val("");
            $("#create-item").find("input[name='price']").val("");
            $("#create-item").find("select[name='available']").val("");
            $("#create-item").find("textarea[name='description']").val("");
            $("#create-item").find("select[id='idCategoryCreate']").val("")
            manageData();
            toastr.success('Item Created Successfully.', 'Success Alert', {timeOut: 3000});

        });
    
    }
   
});

/* Remove Item */
$("body").on("click",".remove-item",function(){
   
    if (confirm("are you sure to delete ?")) {
        var idProduct = $(this).parent("td").data('id');
        $.ajax({
            dataType: 'json',
            type:'GET',
            url: 'api/products/delete.php',
            data:{idProduct:idProduct}
        }).done(function(data){
            manageData(); 
            toastr.success('Item Deleted Successfully.', 'Success Alert', {timeOut: 3000});
        });    
    }
    
});

/* Edit Item */
$("body").on("click",".edit-item",function(){
   
    var idProduct = $(this).parent("td").data('id');

     $.ajax({
                type:'GET',
                dataType: 'json',              
                url:  "api/products/getProductById.php",
                data:{idProduct:idProduct}
    }).done(function(item){
        
        $("#edit-item").find("input[name='title']").val(item.data[0].title);
        $("#edit-item").find("input[name='abstract']").val(item.data[0].abstract);
        $("#edit-item").find("input[name='price']").val(item.data[0].price);
        $("#edit-item").find("textarea[name='description']").val(item.data[0].description);
        $("#edit-item").find("select[id='idCategoryUpdate']").val(item.data[0].idCategory);
        $("#edit-item").find("select[name='available']").val(item.data[0].available);
        $("#edit-item").find(".edit-id").val(item.data[0].idProduct);            
    });    

});

/* Updated new Item */
$(".crud-submit-edit").click(function(e){

        e.preventDefault();
        var form_action = $("#edit-item").find("form").attr("action");
        var title = $("#edit-item").find("input[name='title']").val();
        var abstract = $("#edit-item").find("input[name='abstract']").val();
        var price = $("#edit-item").find("input[name='price']").val();
        var description = $("#edit-item").find("textarea[name='description']").val();
        var idCategoryUpdate = $("#edit-item").find("select[id='idCategoryUpdate']").val();
        var available = $("#edit-item").find("select[name='available']").val();

        var id = $("#edit-item").find(".edit-id").val();

            $.ajax({
                dataType: 'json',
                type:'GET',
                url:  form_action,
                data:{title:title, abstract:abstract,price:price,description:description,idCategory:idCategoryUpdate,available:available, idProduct:id}
            }).done(function(data){
                manageData(); 
                $(".modal").modal('hide');
                toastr.success('Item Updated Successfully.', 'Success Alert', {timeOut: 3000});
            });

});



// combobox
$.ajax({        
    url: "api/category/getData.php",
    dataType: "json",
    success: function (categories) {
        $.each(categories.data,function(i,category){
            var div_data="<option value="+category.idCategory+">"+category.Name+"</option>";
            $(div_data).appendTo('#idCategoryCreate');
            $(div_data).appendTo('#idCategoryUpdate');
        });
    }
});



});


// TO DO  
// INSERT IMAGE 
// FORM VALIDATOR 
// pagination
// search
// menu back 
// category
